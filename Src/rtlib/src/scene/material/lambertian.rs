use rand::prelude::ThreadRng;

use crate::core::{Color, Ray};
use crate::scene::HitResult;
use super::{Material, ScatterResult};
use crate::core::utils::random_in_unit_sphere;

pub struct Lambertian {
    albedo: Color
}

impl Lambertian {
    pub fn new(albedo: &Color) -> Lambertian {
        Lambertian {
            albedo: albedo.clone()
        }
    }
}

impl Material for Lambertian {
    fn scatter(&self, _ray: &Ray, hit_result: &HitResult, rng: &mut ThreadRng) -> Option<ScatterResult> {
        let rand_vec = random_in_unit_sphere(rng);
        let target = &(&hit_result.point + &hit_result.normal) + &rand_vec;

        let scattered = Ray::new_target(&hit_result.point, &target);

        Some(
            ScatterResult {
                attenuation: self.albedo.clone(),
                scattered
            }
        )
    }
}