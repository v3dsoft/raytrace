use rand::prelude::ThreadRng;

use crate::core::{Color, Ray};
use super::{Material, ScatterResult};
use crate::scene::HitResult;
use crate::core::utils::random_in_unit_sphere;

pub struct Metal {
    albedo: Color,
    fuzziness: f32
}

impl Metal {
    pub fn new(albedo: &Color, fuzziness: f32) -> Metal {
        let fixed_fuzz = fuzziness.max(0.0).min(1.0);

        Metal {
            albedo: albedo.clone(),
            fuzziness: fixed_fuzz
        }
    }
}

impl Material for Metal {
    fn scatter(&self, ray: &Ray, hit_result: &HitResult, rng: &mut ThreadRng) -> Option<ScatterResult> {
        let reflected = ray.direction().normalize().reflect(&hit_result.normal);
        let fuzz_reflected = &reflected + &(&random_in_unit_sphere(rng) * self.fuzziness);

        if (&fuzz_reflected * &hit_result.normal) > 0.0 {
            let scattered = Ray::new(&hit_result.point, &fuzz_reflected);

            Some(ScatterResult {
                attenuation: self.albedo.clone(),
                scattered
            })
        } else {
            None
        }
    }
}
