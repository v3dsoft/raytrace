pub mod geometry;

use crate::core::{Vector, Ray};

use super::MaterialId;

pub struct HitResult {
    pub t: f32,
    pub point: Vector,
    pub normal: Vector,
    pub material: MaterialId
}

pub trait Hitable {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<HitResult>;
}
