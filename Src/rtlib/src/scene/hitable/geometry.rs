mod sphere;
pub use sphere::Sphere;

use crate::scene::Hitable;

pub trait Geometry: Hitable {}