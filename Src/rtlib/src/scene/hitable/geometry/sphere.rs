use crate::core::{
    Vector,
    Ray,
    math_fn::{
        solve_quadratic_eq,
        QuadraticEqRoot
    }
};
use crate::scene::{Hitable, HitResult, MaterialId};
use crate::core::math_fn::F32Funcs;
use crate::scene::hitable::geometry::Geometry;

#[derive(Clone)]
pub struct Sphere {
    center: Vector,
    radius: f32,
    material: MaterialId
}

impl Sphere {
    pub fn new(center: &Vector, radius: f32, material: MaterialId) -> Sphere {
        Sphere {
            center: center.clone(),
            radius,
            material
        }
    }

    fn generate_hit_result(&self, ray: &Ray, t: f32) -> HitResult {
        let point = ray.point_at(t);
        let normal = (&point - &self.center).normalize();

        HitResult {
            t,
            point,
            normal,
            material: self.material
        }
    }
}

impl Hitable for Sphere {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<HitResult> {
        let oc = &ray.origin() - &self.center;

        let a = &ray.direction() * &ray.direction();
        let b = 2.0 * (&oc * &ray.direction());
        let c = &oc * &oc - self.radius * self.radius;

        match solve_quadratic_eq(a, b, c) {
            QuadraticEqRoot::Two(t1, t2) => {
                let t = t1.min(t2);

                if t.approx_ge(t_min) && t.approx_le(t_max) {
                    Some(self.generate_hit_result(ray, t))
                } else {
                    None
                }
            },

            QuadraticEqRoot::One(t) => {
                if t.approx_ge(t_min) && t.approx_le(t_max) {
                    Some(self.generate_hit_result(ray, t))
                } else {
                    None
                }
            },

            QuadraticEqRoot::None => None
        }
    }
}

impl Geometry for Sphere {}
