mod lambertian;
pub use lambertian::Lambertian;

mod metal;
pub use metal::Metal;

use rand::prelude::ThreadRng;

use crate::core::{Ray, Color};
use crate::scene::HitResult;

pub struct ScatterResult {
    pub attenuation: Color,
    pub scattered: Ray
}

pub trait Material {
    fn scatter(&self, ray: &Ray, hit_result: &HitResult, rng: &mut ThreadRng) -> Option<ScatterResult>;
}