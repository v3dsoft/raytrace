mod hitable;
pub mod material;

pub(crate) use hitable::HitResult;
pub(crate) use hitable::Hitable;

pub use hitable::geometry;
use geometry::Geometry;

use crate::core::Ray;

use self::material::Material;

#[derive(Copy, Clone)]
pub struct MaterialId(usize);

pub struct Scene {
    geom_list: Vec<Box<dyn Geometry>>,
    material_list: Vec<Box<dyn Material>>
}

impl Scene {
    pub fn new() -> Scene {
        Scene {
            geom_list: Vec::new(),
            material_list: Vec::new()
        }
    }

    pub fn add_geometry(&mut self, g: impl Geometry + 'static) {
        self.geom_list.push(Box::new(g));
    }

    pub fn register_material(&mut self, m: impl Material + 'static) -> MaterialId {
        let id = MaterialId(self.material_list.len());

        self.material_list.push(Box::new(m));

        id
    }

    pub(crate) fn material(&self, id: MaterialId) -> &dyn Material {
        self.material_list[id.0].as_ref()
    }
}

impl Hitable for Scene {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<HitResult> {
        let mut closest_t = t_max;
        let mut result = None;

        for geom in &self.geom_list {
            if let Some(curr_result) = geom.hit(ray, t_min, closest_t) {
                closest_t = curr_result.t;
                result = Some(curr_result);
            }
        }

        result
    }
}

