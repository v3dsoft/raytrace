pub mod core;
pub mod image;
pub mod camera;
pub mod integrator;
pub mod scene;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
