pub mod codecs;

use std::{
    alloc,
    mem
};

use failure::Fail;

use crate::core::Color;

#[derive(Debug, Fail)]
pub enum ImageError {
    #[fail(display = "Image size must be greater than 0")]
    SizeError,

    #[fail(display = "Internal error")]
    InternalError,

    #[fail(display = "Invalid coordinates ({}, {}) on image {}x{}", x, y, width, height)]
    CoordinatesError {
        x: u32,
        y: u32,
        width: u32,
        height: u32
    }
}

pub struct Image {
    width: u32,
    height: u32,

    data: *mut Color
}

impl Image {
    fn init_data_layout(width: u32, height: u32) -> Result<alloc::Layout, ImageError> {
        let pixel_count = (width * height) as usize;
        let data_size  = pixel_count * mem::size_of::<Color>();

        match alloc::Layout::from_size_align(data_size, 4) {
            Ok(layout) => Ok(layout),
            Err(_) => Err(ImageError::InternalError)
        }
    }

    fn init_data(width: u32, height: u32) -> Result<*mut Color, ImageError> {
        if (width == 0) || (height == 0) {
            return Err(ImageError::SizeError);
        }

        let layout = Self::init_data_layout(width, height)?;

        unsafe {
            let data = alloc::alloc(layout) as *mut Color;

            if !data.is_null() {
                Ok(data)
            } else {
                Err(ImageError::InternalError)
            }
        }
    }

    pub fn new(width: u32, height: u32) -> Result<Image, ImageError> {
        let data = Self::init_data(width, height)?;

        Ok(Image {
            width,
            height,

            data
        })
    }

    pub fn width(&self) -> u32 {
        self.width
    }

    pub fn height(&self) -> u32 {
        self.height
    }

    fn check_coordinates(&self, x: u32, y: u32) -> Result<(), ImageError> {
        if (x > self.width) || (y > self.height) {
            return Err(ImageError::CoordinatesError {
                x,
                y,
                width: self.width,
                height: self.height
            });
        }

        Ok(())
    }

    pub fn set_pixel(&mut self, x: u32, y: u32, value: &Color) -> Result<(), ImageError> {
        self.check_coordinates(x, y)?;

        unsafe {
            let offset = (x + self.width * y) as usize;
            let pixel_ptr = self.data.add(offset);

            let pixel = pixel_ptr.as_mut().expect("Data pointer must not be NULL");
            *pixel = value.clone();
        }

        Ok(())
    }

    pub fn set_pixel_gamma2(&mut self, x: u32, y: u32, value: &Color) -> Result<(), ImageError> {
        let gamma_value = Color::new_xrgb(
            value.r.sqrt(),
            value.g.sqrt(),
            value.b.sqrt()
        );

        self.set_pixel(x, y, &gamma_value)
    }

    pub fn pixel(&self, x: u32, y: u32) -> Result<Color, ImageError> {
        unsafe {
            let offset = (x + self.width * y) as usize;
            let pixel_ptr = self.data.add(offset);

            let pixel = pixel_ptr.as_ref().expect("Data pointer must not be NULL");

            Ok(pixel.clone())
        }
    }
}

impl Drop for Image {
    fn drop(&mut self) {
        let layout = Self::init_data_layout(self.width, self.height)
            .expect("Existing image must be correctly set up");

        unsafe {
            alloc::dealloc(self.data as *mut u8, layout);
        }
    }
}