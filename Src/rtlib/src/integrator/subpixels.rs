use failure::Fail;
use rand::Rng;
use crate::core::Color;

#[derive(Fail, Debug)]
pub enum SubpixelPatternError {
    #[fail(display = "Subpixel count must be greater than 0")]
    InvalidSubpixelCount = 0
}

pub struct SubpixelOffset {
    pub du: f32,
    pub dv: f32
}

pub struct SubpixelPattern {
    subpixel_list: Vec<SubpixelOffset>
}

impl SubpixelPattern {
    pub fn new(subpixel_count: u32) -> Result<SubpixelPattern, SubpixelPatternError> {
        if subpixel_count == 0 {
            return Err(SubpixelPatternError::InvalidSubpixelCount);
        }

        let mut subpixel_list = Vec::new();
        if subpixel_count > 1 {
            let mut rng = rand::thread_rng();

            for _ in 0..subpixel_count {
                let du = rng.gen::<f32>() - 0.5;
                let dv = rng.gen::<f32>() - 0.5;

                subpixel_list.push(SubpixelOffset { du, dv });
            }
        } else {
            subpixel_list.push(SubpixelOffset {
                du: 0.0,
                dv: 0.0
            });
        }

        Ok(SubpixelPattern {
            subpixel_list
        })
    }

    pub fn subpixel_count(&self) -> u32 {
        self.subpixel_list.len() as u32
    }

    pub fn iterate_subpixels(&self) -> impl Iterator<Item = &SubpixelOffset> + '_ {
        (&self.subpixel_list).into_iter()
    }
}

pub struct SubpixelAccumulator {
    color: Color,
    count: u32
}

impl SubpixelAccumulator {
    pub fn new() -> SubpixelAccumulator {
        SubpixelAccumulator {
            color: Color::new_xrgb(0.0, 0.0, 0.0),
            count: 0
        }
    }

    pub fn add(&mut self, color: &Color) {
        self.color.r += color.r;
        self.color.g += color.g;
        self.color.b += color.b;

        self.count += 1;
    }

    pub fn result(self) -> Color {
        if self.count > 0 {
            let rev_count = 1.0 / (self.count as f32);

            Color::new_xrgb(
                self.color.r * rev_count,
                self.color.g * rev_count,
                self.color.b * rev_count
            )
        } else {
            Color::new_xrgb(0.0, 0.0, 0.0)
        }
    }
}
