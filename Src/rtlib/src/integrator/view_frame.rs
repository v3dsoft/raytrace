use std::iter::Iterator;

use crate::core::{Vector, Ray};
use crate::image::Image;
use crate::camera::Camera;

pub struct ImagePlane {
    origin: Vector,
    x_axis: Vector,
    y_axis: Vector,

    pixel_x_count: u32,
    pixel_y_count: u32,

    pixel_x_size: f32,
    pixel_y_size: f32
}

impl ImagePlane {
    fn camera_frame(camera: &Camera) -> [Vector; 3] {
        let dir = camera.direction().normalize();
        let src_up = camera.up_vector().normalize();

        let right = Vector::cross(&dir, &src_up).normalize();
        let up  = Vector::cross(&right, &dir).normalize();

        [
            right,
            dir,
            up
        ]
    }

    fn plane_half_size(target: &Image, camera: &Camera) -> (f32, f32) {
        let half_v_angle = camera.vertical_angle() * 0.5;
        let half_h = half_v_angle.tan();

        let aspect = (target.width() as f32) / (target.height() as f32);
        let half_w = half_h * aspect;

        (half_w, half_h)
    }

    pub fn new(target: &Image, camera: &Camera) -> ImagePlane {
        let cam_frame = Self::camera_frame(camera);
        let plane_half_size = Self::plane_half_size(target, camera);

        let plane_center = &camera.position() + &cam_frame[1];

        let origin = &(&plane_center - &(&cam_frame[0] * plane_half_size.0)) + &(&cam_frame[2] * plane_half_size.1);
        let x_axis = &cam_frame[0] * (plane_half_size.0 * 2.0);
        let y_axis = &cam_frame[2] * (-plane_half_size.1 * 2.0);

        let pixel_x_count = target.width();
        let pixel_y_count = target.height();

        let pixel_x_size = 1.0 / (pixel_x_count as f32);
        let pixel_y_size = 1.0 / (pixel_y_count as f32);

        ImagePlane {
            origin,
            x_axis,
            y_axis,

            pixel_x_count,
            pixel_y_count,

            pixel_x_size,
            pixel_y_size
        }
    }

    pub fn param_point(&self, u: f32, v: f32) -> Vector {
        &self.origin + &(&(&self.x_axis * u) + &(&self.y_axis * v))
    }
}

pub struct ViewFrame {
    origin: Vector,
    image_plane: ImagePlane
}

impl ViewFrame {
    pub fn new(target: &Image, camera: &Camera) -> ViewFrame {
        let origin = camera.position();
        let image_plane = ImagePlane::new(target, camera);

        ViewFrame {
            origin,
            image_plane
        }
    }

    pub fn iterate_pixel_rays(&self) -> RayIterator {
        RayIterator::new(self)
    }

    pub fn iterate_pixels(&self) -> PixelIterator {
        PixelIterator::new(self)
    }

    fn camera_ray(&self, u: f32, v: f32) -> Ray {
        let image_point = self.image_plane.param_point(u, v);

        Ray::new_target(&self.origin, &image_point)
    }
}

pub struct RayIterator<'a> {
    view_frame: &'a ViewFrame,
    pixel_x: u32,
    pixel_y: u32
}

impl<'a> RayIterator<'a> {
    fn new(view_frame: &ViewFrame) -> RayIterator {
        RayIterator {
            view_frame,
            pixel_x: 0,
            pixel_y: 0
        }
    }

    fn ray_for_pixel(&self, x: u32, y: u32, sub_x: f32, sub_y: f32) -> Ray {
        let image_plane = &self.view_frame.image_plane;

        let ray_u = ((x as f32) + sub_x) * image_plane.pixel_x_size;
        let ray_v = ((y as f32) + sub_y) * image_plane.pixel_y_size;

        self.view_frame.camera_ray(ray_u, ray_v)
    }
}

impl<'a> Iterator for RayIterator<'a> {
    type Item = (u32, u32, Ray);

    fn next(&mut self) -> Option<Self::Item> {
        if self.pixel_y >= self.view_frame.image_plane.pixel_y_count {
            return None;
        }

        let curr_x = self.pixel_x;
        let curr_y = self.pixel_y;
        let ray = self.ray_for_pixel(curr_x, curr_y, 0.5, 0.5);

        self.pixel_x += 1;
        if self.pixel_x >= self.view_frame.image_plane.pixel_x_count {
            self.pixel_x = 0;
            self.pixel_y += 1;
        }

        Some((curr_x, curr_y, ray))
    }
}

pub struct ViewFramePixel<'a> {
    view_frame: &'a ViewFrame,
    u: f32,
    v: f32
}

impl<'a> ViewFramePixel<'a> {
    fn new(view_frame: &ViewFrame, u: f32, v: f32) -> ViewFramePixel {
        ViewFramePixel { view_frame, u, v }
    }

    pub fn subpixel_ray(&self, du: f32, dv: f32) -> Ray {
        let ray_u = self.u + du * self.view_frame.image_plane.pixel_x_size;
        let ray_v = self.v + dv * self.view_frame.image_plane.pixel_y_size;

        self.view_frame.camera_ray(ray_u, ray_v)
    }
}

pub struct PixelIterator<'a> {
    view_frame: &'a ViewFrame,
    pixel_x: u32,
    pixel_y: u32
}

impl<'a> PixelIterator<'a> {
    fn new(view_frame: &ViewFrame) -> PixelIterator {
        PixelIterator {
            view_frame,
            pixel_x: 0,
            pixel_y: 0
        }
    }
}

impl<'a> Iterator for PixelIterator<'a> {
    type Item = (u32, u32, ViewFramePixel<'a>);

    fn next(&mut self) -> Option<Self::Item> {
        if self.pixel_y >= self.view_frame.image_plane.pixel_y_count {
            return None;
        }

        let curr_x = self.pixel_x;
        let curr_y = self.pixel_y;

        let u = (self.pixel_x as f32 + 0.5) * self.view_frame.image_plane.pixel_x_size;
        let v = (self.pixel_y as f32 + 0.5) * self.view_frame.image_plane.pixel_y_size;
        let pixel = ViewFramePixel::new(self.view_frame, u, v);

        self.pixel_x += 1;
        if self.pixel_x >= self.view_frame.image_plane.pixel_x_count {
            self.pixel_x = 0;
            self.pixel_y += 1;
        }

        Some((curr_x, curr_y, pixel))
    }
}
