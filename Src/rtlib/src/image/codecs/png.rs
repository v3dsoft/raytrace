use std::path::Path;
use std::fs::File;
use std::io::BufWriter;

use png::Writer;

use super::{
    super::Image,
    ImageCodec,
    ImageCodecError
};

pub struct PngCodec {}

impl PngCodec {
    pub fn new() -> Result<PngCodec, ImageCodecError> {
        Ok(PngCodec {})
    }

    fn open_write_file(&self, path: &Path) -> Result<File, ImageCodecError> {
        let file = File::create(path);

        match file {
            Ok(f) => Ok(f),
            Err(err) => return Err(ImageCodecError::FileError(err.to_string()))
        }
    }

    fn write_data(&self, image: &Image, writer: &mut Writer<BufWriter<File>>) -> Result<(), ImageCodecError> {
        let pixel_data_size = (image.width() * image.height() * 4) as usize;
        let mut pixel_data = Vec::with_capacity(pixel_data_size);

        let mut pixel = [0u8; 4];
        pixel[3] = 255; //Alpha is always full

        for y in 0u32..image.height() {
            for x in 0u32..image.width() {
                let img_pixel = match image.pixel(x, y) {
                    Ok(img_pixel) => img_pixel,
                    Err(err) => return Err(ImageCodecError::DataReadError(
                        format!("{}", err)
                    ))
                };

                pixel_data.push((img_pixel.r * 255.0) as u8);
                pixel_data.push((img_pixel.g * 255.0) as u8);
                pixel_data.push((img_pixel.b * 255.0) as u8);
                pixel_data.push(255);
            }
        }

        match writer.write_image_data(pixel_data.as_slice()) {
            Ok(()) => Ok(()),
            Err(err) => Err(ImageCodecError::InternalError(err.to_string()))
        }
    }
}

impl ImageCodec for PngCodec {
    fn can_write(&self) -> bool { true }

    fn write_file(&self, image: &Image, path: &str) -> Result<(), ImageCodecError> {
        let file_path = Path::new(path);
        let file = self.open_write_file(&file_path)?;
        let buf = BufWriter::new(file);

        let mut encoder = png::Encoder::new(buf, image.width(), image.height());
        encoder.set_color(png::ColorType::RGBA);
        encoder.set_depth(png::BitDepth::Eight);

        let mut writer = match encoder.write_header() {
            Ok(writer) => writer,
            Err(err) => return Err(ImageCodecError::InternalError(err.to_string()))
        };

        self.write_data(image, &mut writer)?;

        Ok(())
    }
}