mod png;
pub use self::png::*;

use failure::Fail;

use super::Image;

#[derive(Debug, Fail)]
pub enum ImageCodecError {
    #[fail(display = "This operation is not implemented")]
    NotImplemented,

    #[fail(display = "Internal error: {}", _0)]
    InternalError(String),

    #[fail(display = "File access error: {}", _0)]
    FileError(String),

    #[fail(display = "Data read error: {}", _0)]
    DataReadError(String),
}

pub trait ImageCodec {
    fn can_read(&self) -> bool { false }
    fn can_write(&self) -> bool { false }

    fn read_file(&self, _path: &str) -> Result<Image, ImageCodecError> {
        Err(ImageCodecError::NotImplemented)
    }

    fn write_file(&self, _image: &Image, _path: &str) -> Result<(), ImageCodecError> {
        Err(ImageCodecError::NotImplemented)
    }
}
