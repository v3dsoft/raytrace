use failure::Fail;

use crate::core::Vector;
use crate::core::math_fn::EPSILON;

#[derive(Debug, Fail)]
pub enum CameraError {
    #[fail(display = "Direction vector is zero-length")]
    InvalidDir,

    #[fail(display = "Up vector is zero-length or collinear with direction")]
    InvalidUp,

    #[fail(display = "Vertical angle must be greater than 0")]
    InvalidVAngle,
}

pub struct Camera {
    pos: Vector,
    dir: Vector,
    up: Vector,
    v_angle: f32
}

impl Camera {
    pub fn new(pos: &Vector, dir: &Vector, up: &Vector, v_angle: f32) -> Result<Camera, CameraError> {
        if dir.length() < EPSILON {
            return Err(CameraError::InvalidDir);
        }

        if up.length() < EPSILON {
            return Err(CameraError::InvalidUp);
        }

        if Vector::collinear(dir, up) {
            if dir.length() < EPSILON {
                return Err(CameraError::InvalidUp);
            }
        }

        if v_angle < EPSILON {
            return Err(CameraError::InvalidVAngle);
        }

        Ok(Camera {
            pos: pos.clone(),
            dir: dir.clone(),
            up: up.clone(),
            v_angle
        })
    }

    pub fn position(&self) -> Vector {
        self.pos.clone()
    }

    pub fn direction(&self) -> Vector {
        self.dir.clone()
    }

    pub fn up_vector(&self) -> Vector {
        self.up.clone()
    }

    pub fn vertical_angle(&self) -> f32 {
        self.v_angle
    }
}