mod view_frame;
mod subpixels;

use std::f32;

use failure::Error;
use rand::prelude::ThreadRng;

use crate::core::{Ray, Color};
use crate::core::math_fn::EPSILON;

use crate::image::Image;
use crate::camera::Camera;

use self::view_frame::ViewFrame;
use crate::scene::{Hitable, Scene};
use crate::integrator::subpixels::{SubpixelPattern, SubpixelAccumulator};

pub struct Integrator<'a> {
    target: &'a mut Image,
    frame: ViewFrame,
    subpixel_pattern: SubpixelPattern,
    ray_depth: u32
}

impl<'a> Integrator<'a> {
    pub fn new(target: &'a mut Image, camera: &Camera) -> Integrator<'a> {
        let frame = ViewFrame::new(target, camera);

        let subpixel_pattern = SubpixelPattern::new(1)
            .expect("One subpixel should always work");

        Integrator {
            target,
            frame,
            subpixel_pattern,
            ray_depth: 50
        }
    }

    pub fn subpixel_count(&self) -> u32 {
        self.subpixel_pattern.subpixel_count()
    }

    pub fn set_subpixel_count(&mut self, subpixel_count: u32) -> Result<(), Error> {
        self.subpixel_pattern = SubpixelPattern::new(subpixel_count)?;

        Ok(())
    }

    pub fn ray_depth(&self) -> u32 {
        self.ray_depth
    }

    pub fn set_ray_depth(&mut self, ray_depth: u32) {
        self.ray_depth = ray_depth;
    }

    fn calc_color(&self, scene: &Scene, ray: &Ray, depth: u32, rng: &mut ThreadRng) -> Color {
        if let Some(hit_data) = scene.hit(ray, EPSILON, f32::MAX) {
            if depth < self.ray_depth {
                let material = scene.material(hit_data.material);

                if let Some(scatter) = material.scatter(ray, &hit_data, rng) {
                    Color::multiply_xrgb(
                        &scatter.attenuation,
                        &self.calc_color(scene, &scatter.scattered, depth + 1, rng)
                    )
                } else {
                    Color::new_xrgb(0.0, 0.0, 0.0)
                }
            } else {
                Color::new_xrgb(0.0, 0.0, 0.0)
            }
        } else {
            let unit_dir = ray.direction().normalize();
            let t = 0.5 * (unit_dir.z + 1.0);

            Color::lerp(
                &Color::new_xrgb(1.0, 1.0, 1.0),
                &Color::new_xrgb(0.5, 0.7, 1.0),
                t
            )
        }
    }

    pub fn render(&mut self, scene: &Scene) {
        let mut rng = rand::thread_rng();

        if self.subpixel_pattern.subpixel_count() > 1 {
            for (x, y, vf_pixel) in self.frame.iterate_pixels() {
                let mut accumulator = SubpixelAccumulator::new();

                for subpixel in self.subpixel_pattern.iterate_subpixels() {
                    let ray = vf_pixel.subpixel_ray(subpixel.du, subpixel.dv);

                    let pixel = self.calc_color(scene, &ray, 0, &mut rng);
                    accumulator.add(&pixel);
                }

                self.target.set_pixel_gamma2(x, y, &accumulator.result()).expect("INTERNAL ERROR: Integrator goes out of image bounds");
            }
        } else {
            for (x, y, ray) in self.frame.iterate_pixel_rays() {
                let pixel = self.calc_color(scene, &ray, 0, &mut rng);
                self.target.set_pixel_gamma2(x, y, &pixel).expect("INTERNAL ERROR: Integrator goes out of image bounds");
            }
        }
    }
}