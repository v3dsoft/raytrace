use rand::prelude::ThreadRng;
use rand::Rng;

use crate::core::Vector;
use crate::core::math_fn::F32Funcs;

pub fn random_in_unit_sphere(rng: &mut ThreadRng) -> Vector {
    loop {
        let mut point = Vector::new(rng.gen(), rng.gen(), rng.gen());
        point *= 2.0;
        point -= &Vector::new(1.0, 1.0, 1.0);

        if point.length_sq().approx_le(1.0) {
            return point;
        }
    }
}