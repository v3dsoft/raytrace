pub const EPSILON: f32 = 0.0001;

pub trait F32Funcs {
    fn approx_eq(self, rhs: f32) -> bool;
    fn approx_greater(self, rhs: f32) -> bool;
    fn approx_ge(self, rhs: f32) -> bool;
    fn approx_less(self, rhs: f32) -> bool;
    fn approx_le(self, rhs: f32) -> bool;

    fn rsqrt(self) -> f32;
}

impl F32Funcs for f32 {
    fn approx_eq(self, rhs: f32) -> bool { (self - rhs).abs() < EPSILON }
    fn approx_greater(self, rhs: f32) -> bool { self > rhs - EPSILON }
    fn approx_ge(self, rhs: f32) -> bool { self >= rhs - EPSILON }
    fn approx_less(self, rhs: f32) -> bool { self < rhs + EPSILON }
    fn approx_le(self, rhs: f32) -> bool { self < rhs + EPSILON }

    fn rsqrt(self) -> f32 {
        #[cfg(target_arch = "x86_64")]
        use std::arch::x86_64::{
            _mm_load_ss,
            _mm_store_ss,
            _mm_rsqrt_ss
        };

        #[cfg(target_arch = "x86")]
        use std::arch::x86::{
            _mm_load_ss,
            _mm_store_ss,
            _mm_rsqrt_ss
        };

        unsafe {
            let val = _mm_load_ss(&self);
            let res = _mm_rsqrt_ss(val);

            let mut fres: f32 = 0.0;
            _mm_store_ss(&mut fres, res);

            fres
        }
    }
}

pub enum QuadraticEqRoot {
    None,
    One(f32),
    Two(f32, f32)
}

pub fn solve_quadratic_eq(a: f32, b: f32, c: f32) -> QuadraticEqRoot {
    let d = b * b - 4.0 * a * c;

    if d > EPSILON {
        let root_d = d.sqrt();
        let rev_two_a = 1.0 / (2.0 * a);

        QuadraticEqRoot::Two(
            (-b + root_d) * rev_two_a,
            (-b - root_d) * rev_two_a
        )
    } else if d < -EPSILON {
        QuadraticEqRoot::None
    } else {
        QuadraticEqRoot::One(-b / (2.0 * a))
    }
}