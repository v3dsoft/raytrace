use super::math_fn::F32Funcs;

pub type XRGB = u32;

pub fn xrgb_from_components(r: u8, g: u8, b: u8) -> XRGB {
    ((r as u32) << 16) + ((g as u32) << 8) + (b as u32)
}

#[derive(Clone)]
pub struct Color {
    pub a: f32,
    pub r: f32,
    pub g: f32,
    pub b: f32
}

impl Color {
    pub const fn new_xrgb(r: f32, g: f32, b: f32) -> Color {
        Color {a: 1.0, r, g, b}
    }

    pub fn to_xrgb(&self) -> XRGB {
        let mut xrgb = 0x0000ff00 as XRGB;

        let r = (self.r * 255.0f32) as XRGB;
        xrgb += r;

        let g = (self.g * 255.0f32) as XRGB;
        xrgb <<= 8;
        xrgb += g;

        let b = (self.b * 255.0f32) as XRGB;
        xrgb <<= 8;
        xrgb += b;

        xrgb
    }

    pub fn lerp(c1: &Color, c2: &Color, t: f32) -> Color {
        let one_minus_t = 1.0 - t;

        Color {
            r: one_minus_t * c1.r + t * c2.r,
            g: one_minus_t * c1.g + t * c2.g,
            b: one_minus_t * c1.b + t * c2.b,
            a: one_minus_t * c1.a + t * c2.a
        }
    }

    pub fn multiply_xrgb(c1: &Color, c2: &Color) -> Color {
        Color {
            r: c1.r * c2.r,
            g: c1.g * c2.g,
            b: c1.b * c2.b,
            a: 1.0
        }
    }
}

impl PartialEq for Color {
    fn eq(&self, other: &Color) -> bool {
        self.a.approx_eq(other.a) &&
            self.r.approx_eq(other.r) &&
            self.g.approx_eq(other.g) &&
            self.b.approx_eq(other.b)
    }
}

impl Eq for Color {}