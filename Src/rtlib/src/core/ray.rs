use super::Vector;

#[derive(Debug, Clone)]
pub struct Ray {
    origin: Vector,
    dir: Vector,
}

impl Ray {
    pub fn new(origin: &Vector, dir: &Vector) -> Ray {
        Ray {
            origin: (*origin).clone(),
            dir: (*dir).clone()
        }
    }

    pub fn new_target(origin: &Vector, target: &Vector) -> Ray {
        let dir = target - origin;

        Ray {
            origin: (*origin).clone(),
            dir
        }
    }

    pub fn origin(&self) -> Vector {
        self.origin.clone()
    }

    pub fn direction(&self) -> Vector { self.dir.clone() }

    pub fn point_at(&self, t: f32) -> Vector {
        &self.origin + &(&self.dir * t)
    }
}
