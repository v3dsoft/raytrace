use std::{ cmp, ops };

use super::math_fn::{
    EPSILON,
    F32Funcs
};

#[derive(Debug, Clone)]
pub struct Vector {
    pub x: f32,
    pub y: f32,
    pub z: f32
}

impl Vector {
    pub fn new(x: f32, y: f32, z: f32) -> Vector {
        Vector { x, y, z }
    }

    pub fn length(&self) -> f32 {
        (self.x * self.x + self.y * self.y + self.z * self.z).sqrt()
    }

    pub fn length_sq(&self) -> f32 {
        self.x * self.x + self.y * self.y + self.z * self.z
    }

    pub fn normalize(&self) -> Vector {
        let inv_len = self.length_sq().rsqrt();

        if inv_len > EPSILON {
            Vector {
                x: self.x * inv_len,
                y: self.y * inv_len,
                z: self.z * inv_len
            }
        } else {
            Vector {
                x: 0.0,
                y: 0.0,
                z: 0.0
            }
        }
    }

    pub fn normalize_this(&mut self) {
        let inv_len = self.length_sq().rsqrt();

        if inv_len > EPSILON {
            self.x *= inv_len;
            self.y *= inv_len;
            self.z *= inv_len;
        } else {
            self.x = 0.0;
            self.y = 0.0;
            self.z = 0.0;
        }
    }

    pub fn cross(v1: &Vector, v2: &Vector) -> Vector {
        Vector {
            x: v1.y * v2.z - v1.z * v2.y,
            y: v1.z * v2.x - v1.x * v2.z,
            z: v1.x * v2.y - v1.y * v2.x
        }
    }

    pub fn collinear(v1: &Vector, v2: &Vector) -> bool {
        let cross = Vector::cross(v1, v2);

        cross.length_sq() < EPSILON
    }

    pub fn reflect(&self, n: &Vector) -> Vector {
        self - &(n * (2.0 * (self * n)))
    }
}

impl cmp::PartialEq for Vector {
    fn eq(&self, other: &Vector) -> bool {
        self.x.approx_eq(other.x) &&
            self.y.approx_eq(other.y) &&
            self.z.approx_eq(other.z)
    }
}
impl cmp::Eq for Vector {}

impl ops::Neg for &Vector {
    type Output = Vector;

    fn neg(self) -> Vector {
        Vector {
            x: -self.x,
            y: -self.y,
            z: -self.z
        }
    }
}

impl ops::Add<&Vector> for &Vector {
    type Output = Vector;

    fn add(self, rhs: &Vector) -> Vector {
        Vector {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            z: self.z + rhs.z
        }
    }
}

impl ops::AddAssign<&Vector> for Vector {
    fn add_assign(&mut self, rhs: &Vector) {
        self.x += rhs.x;
        self.y += rhs.y;
        self.z += rhs.z;
    }
}

impl ops::Sub<&Vector> for &Vector {
    type Output = Vector;

    fn sub(self, rhs: &Vector) -> Vector {
        Vector {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z
        }
    }
}

impl ops::SubAssign<&Vector> for Vector {
    fn sub_assign(&mut self, rhs: &Vector) {
        self.x -= rhs.x;
        self.y -= rhs.y;
        self.z -= rhs.z;
    }
}

impl ops::Mul<f32> for &Vector {
    type Output = Vector;

    fn mul(self, rhs: f32) -> Vector {
        Vector {
            x: self.x * rhs,
            y: self.y * rhs,
            z: self.z * rhs
        }
    }
}

impl ops::MulAssign<f32> for Vector {
    fn mul_assign(&mut self, rhs: f32) {
        self.x *= rhs;
        self.y *= rhs;
        self.z *= rhs;
    }
}

impl ops::Mul for &Vector {
    type Output = f32;

    fn mul(self, rhs: &Vector) -> f32 {
        self.x * rhs.x + self.y * rhs.y + self.z * rhs.z
    }
}
