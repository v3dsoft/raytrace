use failure::Error;

pub type ErrorResult<T> = Result<T, Error>;
