use failure::{Error, ResultExt};

use rtlib::{
    core::{
        Vector,
        Color
    },
    image::{
        Image,
        codecs::{
            ImageCodec,
            PngCodec
        }
    },
    camera::Camera,
    integrator::Integrator,
    scene::{
        Scene,
        geometry,
        material
    }
};

const EXPORT_FILE: &str = "export.png";

fn export_image(image: &Image) -> Result<(), Error> {
    let codec = PngCodec::new()?;
    codec.write_file(image, EXPORT_FILE)?;

    Ok(())
}

fn build_test_scene() -> Scene {
    let mut scene = Scene::new();

    let ground_mat = material::Lambertian::new(&Color::new_xrgb(0.8, 0.8, 0.0));
    let ground_mat = scene.register_material(ground_mat);

    let mat1 = material::Lambertian::new(&Color::new_xrgb(0.8, 0.3, 0.3));
    let mat1 = scene.register_material(mat1);

    let mat2 = material::Metal::new(&Color::new_xrgb(0.8, 0.6, 0.2), 0.01);
    let mat2 = scene.register_material(mat2);

    let mat3 = material::Metal::new(&Color::new_xrgb(0.8, 0.8, 0.8), 0.8);
    let mat3 = scene.register_material(mat3);

    let sphere1 = geometry::Sphere::new(&Vector::new(0.0, 2.0, 0.0), 0.5, mat1);
    scene.add_geometry(sphere1);

    let sphere2 = geometry::Sphere::new(&Vector::new(1.0, 2.0, 0.0), 0.5, mat2);
    scene.add_geometry(sphere2);

    let sphere3 = geometry::Sphere::new(&Vector::new(-1.0, 2.0, 0.0), 0.5, mat3);
    scene.add_geometry(sphere3);

    let ground_sphere = geometry::Sphere::new(&Vector::new(0.0, 2.0, -100.5), 100.0, ground_mat);
    scene.add_geometry(ground_sphere);

    scene
}

fn render_image(image: &mut Image, scene: &Scene) -> Result<(), Error> {
    let pos = Vector::new(0.0, 0.0, 0.0);
    let dir = Vector::new(0.0, 1.0, 0.0);
    let up = Vector::new(0.0, 0.0, 1.0);

    let camera = Camera::new(&pos, &dir, &up, 60.0f32.to_radians())?;

    let mut integrator = Integrator::new(image, &camera);
    integrator.set_subpixel_count(128)?;

    integrator.render(scene);

    Ok(())
}

fn run() -> Result<(), Error> {
    let mut image = Image::new(1280, 720)
        .context("Error creating test image")?;

    let scene = build_test_scene();

    let start_time = std::time::Instant::now();
    render_image(&mut image, &scene)?;
    println!("Render time: {}ms", start_time.elapsed().as_millis());

    export_image(&image).context("Error exporting result image")?;

    Ok(())
}

fn print_error(e: Error) {
    println!("Error: {}", e);

    let mut cause = e.as_fail().cause();
    while let Some(c) = cause {
        println!("Cause: {}", c);
        cause = c.cause();
    }

    println!();
}

fn main() {
    let err = run();
    if let Err(e) = err {
        print_error(e);
    }
}
